package com.example.monitoringppb.app

import com.example.monitoringppb.model.ResponModel
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @FormUrlEncoded
    @POST("register")
    fun register(
        @Field("name") Name :String,
        @Field("email") Email :String,
        @Field("password") Password :String
    ):Call<ResponModel>

    @FormUrlEncoded
    @POST("auth-login")
    fun login(
        @Field("email") Email :String,
        @Field("password") Password :String
    ):Call<ResponModel>



}