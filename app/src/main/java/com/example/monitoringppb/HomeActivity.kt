package com.example.monitoringppb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
    fun activityKalori(view: View?) {
        val intent = Intent(this@HomeActivity, KaloriActivity::class.java)
        startActivity(intent)
    }
    fun dataDiri(view: View?) {
        val intent = Intent(this@HomeActivity, InformasiDataDiri::class.java)
        startActivity(intent)
    }
    fun panduanMenu(view: View?) {
        val intent = Intent(this@HomeActivity, PanduanMenu::class.java)
        startActivity(intent)
    }


}