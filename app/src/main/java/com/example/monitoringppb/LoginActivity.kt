package com.example.monitoringppb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.content.Intent
import android.widget.Toast
import com.example.monitoringppb.app.ApiConfig
import com.example.monitoringppb.helper.SharedPref

import com.example.monitoringppb.model.ResponModel
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.Call
import okhttp3.Response
import okhttp3.ResponseBody
import javax.security.auth.callback.Callback

class LoginActivity : AppCompatActivity() {
    lateinit var s:SharedPref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        s = SharedPref(this)

        submitLogin.setOnClickListener {
            login()
        }
    }


    fun login(){
        if(editUsername.text.isEmpty()){
            editUsername.error = "Kolom Tidak Boleh Kosong"
            editUsername.requestFocus()
            return
        }else if(editPassword.text.isEmpty()) {
            editPassword.error = "Kolom Password Tidak Boleh Kosong"
            editPassword.requestFocus()
            return
        }

        ApiConfig.instanceRetrofit.login(editUsername.text.toString(),editPassword.text.toString()).enqueue(object : retrofit2.Callback<ResponModel> {
            override fun onFailure(call: retrofit2.Call<ResponModel>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "Error: "+t.message, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: retrofit2.Call<ResponModel>,response: retrofit2.Response<ResponModel>){
                val respon = response.body()!!
                if (respon.Success == 1){
                    s.setStatusLogin(true)
//                    s.setUser(respon.User)

//                    s.setString(s.nama, respon.user.name)
//                    s.setString(s.phone, respon.user.phone)
//                    s.setString(s.email, respon.user.email)

                    val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                    //success
                    Toast.makeText(this@LoginActivity, "Selamat Datang ", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this@LoginActivity, "Error: ", Toast.LENGTH_SHORT).show()
                }
            }

        })
    }


}
